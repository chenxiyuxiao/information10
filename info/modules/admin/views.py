from datetime import datetime, timedelta
import time

from flask import render_template, request, current_app, session, redirect, url_for, jsonify, g, abort

from info import constants, db
from info.models import User, News, Category
from info.modules.admin import admin_blu
from info.utils.common import user_login_data
from info.utils.image_storage import storage
from info.utils.response_code import RET

# 新闻分类的数据展示
@admin_blu.route("/news_type",methods=["GET","POST"])
def news_type():
    if request.method=="GET":
        # 查询分类数据
        try:
            categories=Category.query.all()
        except Exception as e:
            current_app.logger.error(e)
            return render_template('admin/news_type.html',errmsg="查询数据错误")

        category_dict_li=[]
        for category in categories:
            # 取到分类的字典
            category_dict=category.to_dict()
            category_dict_li.append(category_dict)
        # 移除最新的分类
        category_dict_li.pop(0)

        data={
            "categories":category_dict_li
        }
        return render_template('admin/news_type.html',data=data)
    # 新增或者添加分类
    # 1 取参数
    cname=request.json.get("name")
    # 如果传了cid 代表是编辑已存在的分类
    cid=request.json.get("id")
    if not cname:
        return jsonify(errno=RET.PARAMERR,errmsg="参数错误")
    if cid:
        # 有分类id 代表查询相关数据
        try:
            cid =int(cid)
            category=Category.query.get(cid)
        except Exception as e:
            current_app.logger.error(e)
            return jsonify(errno=RET.PARAMERR,errmsg="参数错误")

        if not category:
            return jsonify(errno=RET.NODATA,errmsg="未查询到分类数据")
        category.name=cname
    else:
        category=Category()
        category.name=cname
        db.session.add(category)
    return jsonify(errno=RET.OK,errmsg="OK")


# 新闻的编辑提交
@admin_blu.route("/news_edit_detail", methods=['POST', 'GET'])
def news_edit_detail():
    if request.method == "GET":
        # 查询点击的新闻的相关数据并传入到模板中
        news_id = request.args.get("news_id")

        if not news_id:
            abort(404)
        try:
            news_id = int(news_id)
        except Exception as e:
            current_app.logger.error(e)
            return render_template('admin/news_edit_detail.html', errmsg="参数有误")
        try:
            news = News.query.get(news_id)
        except Exception as e:
            current_app.logger.error(e)
            return render_template("admin/news_edit_detail.html", errmsg="数据查询错误")

        if not news:
            return render_template("admin/news_edit_detail.html", errmsg="未查询到数据")

        # 查询分类
        categories = []
        try:
            categories = Category.query.all()
        except Exception as e:
            current_app.logger.error(e)
            return render_template("admin/news_edit_detail.html", errmsg="数据查询错误")

        category_dict_li = []
        for category in categories:
            # 取到分类的字典
            cate_dict = category.to_dict()
            # 判断当前遍历到的分类是否是当前新闻的分类, 如果是则添加is_select 为True
            if category.id == news.category_id:
                cate_dict["is_selected"] = True
            category_dict_li.append(cate_dict)

        # 移除最新的分类
        category_dict_li.pop(0)

        data = {
            "news":       news.to_dict(),
            "categories": category_dict_li
        }

        return render_template("admin/news_edit_detail.html", data=data)

    # 取到 post 进来的数据
    news_id = request.form.get("news_id")
    title = request.form.get("title")
    digest = request.form.get("digest")
    content = request.form.get("content")
    index_image = request.files.get("index_image")
    category_id = request.form.get("category_id")
    # 1.1 判断数据是否有值
    if not all([title, digest, content, category_id]):
        return jsonify(errno=RET.PARAMERR, errmsg="参数有误")
    # 查询指定id
    try:
        news = News.query.get(news_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="数据查询失败")
    if not news:
        return jsonify(errno=RET.NODATA, errmsg="未查询到新闻数据")
    # 1.2 尝试读取图片
    if index_image:
        try:
            index_image = index_image.read()
        except Exception as e:
            current_app.logger.error(e)
            return jsonify(errno=RET.PARAMERR, errmsg="参数有误")
        # 2 将标题图片上传到七牛
        try:
            key = storage(index_image)
        except Exception as e:
            current_app.logger.error(e)
            return jsonify(errno=RET.THIRDERR, errmsg="上传图片失败")
        news.index_image_url = constants.QINIU_DOMIN_PREFIX + key
    # 3 设置相关数据
    news.title = title
    news.digest = digest
    news.content = content
    news.category_id = category_id
    return jsonify(errno=RET.OK, errmsg="OK")


# 新闻编辑 列表界面
@admin_blu.route("/news_edit")
def news_edit():
    page = request.args.get("p", 1)
    keywords = request.args.get("keywords", None)
    try:
        page = int(page)
    except Exception as e:
        current_app.logger.error(e)
        page = 1
    news_list = []
    current_page = 1
    total_page = 1

    filters = [News.status == 0]
    # 如果关键字存在, 那么就调价关键字搜索
    if keywords:
        filters.append(News.title.contains(keywords))
    try:
        paginate = News.query.filter(*filters).order_by(News.create_time.desc()).paginate(page,
                                                                                          constants.ADMIN_NEWS_PAGE_MAX_COUNT,
                                                                                          False)
        news_list = paginate.items
        current_page = paginate.page
        total_page = paginate.pages
    except Exception as e:
        current_app.logger.error(e)
    news_dict_list = []
    for news in news_list:
        news_dict_list.append((news.to_basic_dict()))
    context = {
        "total_page":   total_page,
        "current_page": current_page,
        "news_list":    news_dict_list
    }
    return render_template("admin/news_edit.html", data=context)


# post 方式提交
@admin_blu.route('/news_review_action', methods=['POST'])
def news_review_action():
    # 1 接受参数
    news_id = request.json.get("news_id")
    action = request.json.get("action")

    # 2 参数校验
    if not all([news_id, action]):
        return jsonify(errno=RET.PARAMERR, errmsg="参数错误")

    if action not in ("accept", "reject"):
        return jsonify(errno=RET.PARAMERR, errmsg="参数有误")
    # 查询到指定的新闻数据
    try:
        news = News.query.get(news_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="数据查询失败")
    if not news:
        return jsonify(errno=RET.NODATA, errmsg="未查询到数据")
    if action == "accept":
        # 代表接受
        news.status = 0

    else:
        # 代表拒绝
        reason = request.json.get('reason')
        if not reason:
            return jsonify(errno=RET.PARAMERR, errmsg="请输入拒绝原因")
        news.status = -1
        news.reason = reason
    return jsonify(errno=RET.OK, errmsg="OK")


@admin_blu.route('/news_review_detail/<int:news_id>')
def news_review_detail(news_id):
    # 通过id查询新闻
    news = None
    try:
        news = News.query.get(news_id)
    except Exception as e:
        current_app.logger.error(e)
    if not news:
        return render_template('admin/news_review_detail.html', data={"errmsg": "没有查询到此新闻"})
    # 返回数据
    data = {"news": news.to_dict()}
    return render_template('admin/news_review_detail.html', data=data)


@admin_blu.route('/news_review')
def news_review():
    page = request.args.get("p", 1)
    keywords = request.args.get("keywords", None)
    try:
        page = int(page)
    except Exception as e:
        current_app.logger.error(e)
        page = 1
    news_list = []
    current_page = 1
    total_page = 1
    filters = [News.status != 0]
    if keywords:
        filters.append(News.title.contains(keywords))
    try:
        paginate = News.query.filter(*filters).order_by(News.create_time.desc()).paginate(page,
                                                                                          constants.ADMIN_NEWS_PAGE_MAX_COUNT,
                                                                                          False)
        news_list = paginate.items
        current_page = paginate.page
        total_page = paginate.pages
    except Exception as e:
        current_app.logger.error(e)
    news_dict_list = []
    for news in news_list:
        news_dict_list.append(news.to_review_dict())
    context = {
        "total_page":   total_page,
        "current_page": current_page,
        "news_list":    news_dict_list
    }
    # print(len(news_list))
    return render_template('admin/news_review.html', data=context)


@admin_blu.route("/user_list")
def user_list():
    page = request.args.get('p', 1)

    try:
        page = int(page)
    except Exception as e:
        current_app.logger.error(e)
        page = 1
    try:
        paginate = User.query.filter(User.is_admin == False).paginate(page, constants.ADMIN_NEWS_PAGE_MAX_COUNT, False)
        users = paginate.items
        current_page = paginate.page
        total_page = paginate.pages
    except Exception as e:
        current_app.logger.error(e)

    # 进行模型列表转成字典
    user_dict_li = []
    for user in users:
        user_dict_li.append(user.to_admin_dict())
    data = {
        "users":        user_dict_li,
        "total_page":   total_page,
        "current_page": current_page
    }

    return render_template('admin/user_list.html', data=data)


@admin_blu.route('/user_count')
def user_count():
    total_count = 0
    # 用户总数, 不包括管理员
    try:
        total_count = User.query.filter(User.is_admin == False).count()
    except Exception as e:
        current_app.logger.error(e)
    mon_count = 0

    #  月  新增数
    t = time.localtime()  # 获取当前事件
    begin_mon_data_str = "%d-%02d-01" % (t.tm_year, t.tm_mon)
    begin_mon_data = datetime.strptime(begin_mon_data_str, "%Y-%m-%d")
    try:
        mon_count = User.query.filter(User.is_admin == False, User.create_time > begin_mon_data).count()
    except Exception as e:
        current_app.logger.error(e)
    day_count = 0

    #  日新增数
    begin_day_data = datetime.strptime(("%d-%02d-%02d" % (t.tm_year, t.tm_mon, t.tm_mday)), "%Y-%m-%d")
    try:
        day_count = User.query.filter(User.is_admin == False, User.create_time > begin_day_data).count()
    except Exception as e:
        current_app.logger.error(e)

    active_time = []
    active_count = []
    # 取到今天的时间字符串 并转成 时间对象
    begin_today_date = datetime.strptime(('%d-%02d-%02d' % (t.tm_year, t.tm_mon, t.tm_mday)), "%Y-%m-%d")

    for i in range(0, 31):
        # 抓到某一天的0点0分
        begin_date = begin_today_date - timedelta(days=i)
        # 取下一天的0点0分
        end_data = begin_today_date - timedelta(days=(i - 1))
        count = User.query.filter(User.is_admin == False, User.last_login >= begin_date,
                                  User.last_login < end_data).count()

        active_count.append(count)
        active_time.append(begin_date.strftime('%Y-%m-%d'))
    data = {
        "total_count":  total_count,
        "mon_count":    mon_count,
        "day_count":    day_count,
        "active_time":  active_time,
        "active_count": active_count
    }
    return render_template('admin/user_count.html', data=data)


@admin_blu.route('logout')
def logout():
    """
    退出登录
    :return:
    """
    session.pop('user_id', None)
    session.pop('mobile', None)
    session.pop('nick_name', None)
    # 如果不清楚 管理员的session 那么管理员先登录,
    # 然后退出 普通用户登陆也能登陆管理员页面
    session.pop('is_admin', None)
    # 自己处理的返回结果
    return redirect('/')
    #  文档给出的返回结果
    # return jsonify(errno=RET.OK, errmsg="OK")


@admin_blu.route('/index')
@user_login_data
def index():
    user = g.user
    return render_template('admin/index.html', user=user.to_dict())


@admin_blu.route('/login', methods=["GET", "POST"])
def login():
    if request.method == "GET":
        # 判断当前用户 是否有登陆, 如果管理员有登陆 直接重定向到管理员后台主页
        user_id = session.get("user_id", None)
        is_admin = session.get("is_admin", False)
        if user_id and is_admin:
            return redirect(url_for('admin.index'))

        return render_template("admin/login.html")

    # 获取登陆的参数
    username = request.form.get("username")
    password = request.form.get("password")

    # 判断参数
    if not all([username, password]):
        return render_template("admin/login.html", errmsg="参数有误")
    try:
        user = User.query.filter(User.mobile == username, User.is_admin == True).first()
    except Exception as e:
        current_app.logger.error(e)
        return render_template('admin/login.html', errmsg='用户信息查询失败')
    if not user:
        return render_template('admin/login.html', errmsg="未查询到用户信息")

    # 校验用户的密码
    if not user.check_passowrd(password):
        return render_template('admin/login.html', errmsg="用户名或者密码错误")

    # 保存用户的登陆信息
    session['user_id'] = user.id
    session['mobile'] = user.mobile
    session['is_admin'] = user.is_admin
    session['nick_name'] = user.nick_name

    # 跳转到后面的管理首页
    return redirect(url_for('admin.index'))
