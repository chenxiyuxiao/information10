# 管理员页面 蓝图
from flask import Blueprint, redirect, session, request, url_for

admin_blu=Blueprint("admin",__name__)

from  . import views

@admin_blu.before_request
def check_admin():
    # 如果不是管理员则重定向到主页
    is_admin=session.get("is_admin",False)
    if not is_admin and not request.url.endswith(url_for('admin.login')):
        return redirect("/")

