# 通过网站检验的相关功能全部放在此文件夹下 和注册登录功能相关的代码都放在这里
from flask import Blueprint

# 创建蓝图对象
passport_blu = Blueprint('passport', __name__,url_prefix="/passport")

from . import views
