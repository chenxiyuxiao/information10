import logging

import redis


class Config(object):
    """工程配置信息"""
    DEBUG = True

    SECRET_KEY = "ojVqAxLuw7Sz/7uMSMy8hWB9RRMtCnWgem7TDomZhPNvjDeGZjfF7+4lFloksbgk"
    # 数据库的配置信息
    SQLALCHEMY_DATABASE_URI = "mysql://root:mysql@127.0.0.1:3306/information"
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    # 在请求结束时候,如果指定此配置为True,那么 SQLAlchemy 会自动执行一次 db.session.commit
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True

    # redis 配置
    REDIS_HOST = "127.0.0.1"
    REDIS_PORT = 6379

    # flask_session 的配置信息
    SESSION_TYPE = "redis"  # 指定session 保存到 redis中
    SESSION_USE_SIGNER = True  # 让cookie 中的 session_id 被加密签名处理
    SESSION_REDIS = redis.StrictRedis(host=REDIS_HOST, port=REDIS_PORT)  # 这里使用redis 的实例
    PERMANENT_SESSION_LIFETIME = 86400  # session 的有效期 ,单位是秒
    # 设置日志等级
    LOG_LEVEL = logging.DEBUG


class DevelopmentConfig(Config):
    """开发环境下的配置"""
    DEBUG = True


class ProductionConfig(Config):
    """生产环境下的配置"""
    DEBUG = False
    LOG_LEVEL = logging.WARNING


class TestingConfig(Config):
    """测试环境下的配置"""
    DEBUG = True
    TESTING = True


config = {
    "development": DevelopmentConfig,
    "production":  ProductionConfig,
    "testing":     TestingConfig
}
